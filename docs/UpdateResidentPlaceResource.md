# HabSDK.Model.UpdateResidentPlaceResource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClimateMode** | **string** |  | [optional] 
**FanSpeed** | **string** |  | [optional] 
**CoolTarget** | **int?** |  | [optional] 
**HeatTarget** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

