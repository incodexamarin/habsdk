# HabSDK.Api.DevicesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateDevice**](DevicesApi.md#createdevice) | **POST** /devices | Create a device
[**DeleteDeviceById**](DevicesApi.md#deletedevicebyid) | **DELETE** /devices/{id} | Delete device by Id
[**DeleteDevicesAlerts**](DevicesApi.md#deletedevicesalerts) | **DELETE** /devices/:id/alerts | Delete the alert status for a device
[**GetDeviceById**](DevicesApi.md#getdevicebyid) | **GET** /devices/{id} | Get device by Id
[**GetDevices**](DevicesApi.md#getdevices) | **GET** /devices | List devices
[**GetDevicesAlerts**](DevicesApi.md#getdevicesalerts) | **GET** /devices/alerts | List devices alerts
[**GetDevicesMetrics**](DevicesApi.md#getdevicesmetrics) | **GET** /devices/metrics | List devices metrics
[**UpdateDeviceById**](DevicesApi.md#updatedevicebyid) | **PUT** /devices/{id} | Update device by Id
[**UpdateDevicesAlerts**](DevicesApi.md#updatedevicesalerts) | **PUT** /devices/:id/alerts | Update the alert status for a device


<a name="createdevice"></a>
# **CreateDevice**
> DeviceResource CreateDevice (DeviceResource deviceResource = null)

Create a device

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreateDeviceExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var deviceResource = new DeviceResource(); // DeviceResource | The new device (optional) 

            try
            {
                // Create a device
                DeviceResource result = apiInstance.CreateDevice(deviceResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.CreateDevice: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceResource** | [**DeviceResource**](DeviceResource.md)| The new device | [optional] 

### Return type

[**DeviceResource**](DeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletedevicebyid"></a>
# **DeleteDeviceById**
> void DeleteDeviceById (string id)

Delete device by Id

Delete a device based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class DeleteDeviceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Delete device by Id
                apiInstance.DeleteDeviceById(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.DeleteDeviceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletedevicesalerts"></a>
# **DeleteDevicesAlerts**
> DeviceResource DeleteDevicesAlerts (AlertResource1 alertResource)

Delete the alert status for a device

Deletes the alert status for a specific device for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class DeleteDevicesAlertsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var alertResource = new AlertResource1(); // AlertResource1 | The device's requested alert status

            try
            {
                // Delete the alert status for a device
                DeviceResource result = apiInstance.DeleteDevicesAlerts(alertResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.DeleteDevicesAlerts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertResource** | [**AlertResource1**](AlertResource1.md)| The device&#39;s requested alert status | 

### Return type

[**DeviceResource**](DeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdevicebyid"></a>
# **GetDeviceById**
> DeviceResource GetDeviceById (string id)

Get device by Id

Returns matched device id

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetDeviceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Get device by Id
                DeviceResource result = apiInstance.GetDeviceById(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.GetDeviceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**DeviceResource**](DeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdevices"></a>
# **GetDevices**
> PageResourceDeviceResource GetDevices (string filter = null, int? size = null, int? page = null, string order = null)

List devices

Returns the list of devices for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetDevicesExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. I.E. `filter=battery_level:100|climate_mode:heat|signal_between:,60|battery_level_between:20,60` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                // List devices
                PageResourceDeviceResource result = apiInstance.GetDevices(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.GetDevices: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. I.E. &#x60;filter&#x3D;battery_level:100|climate_mode:heat|signal_between:,60|battery_level_between:20,60&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourceDeviceResource**](PageResourceDeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdevicesalerts"></a>
# **GetDevicesAlerts**
> PageResourceDeviceResource GetDevicesAlerts (string filter = null, int? size = null, int? page = null, string order = null)

List devices alerts

Returns the list of devices with alerts for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetDevicesAlertsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. I.E. `filter=battery_level:100|climate_mode:heat|signal_between:,60|battery_level_between:20,60` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                // List devices alerts
                PageResourceDeviceResource result = apiInstance.GetDevicesAlerts(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.GetDevicesAlerts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. I.E. &#x60;filter&#x3D;battery_level:100|climate_mode:heat|signal_between:,60|battery_level_between:20,60&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourceDeviceResource**](PageResourceDeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getdevicesmetrics"></a>
# **GetDevicesMetrics**
> void GetDevicesMetrics ()

List devices metrics

Returns metrics by all the devices

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetDevicesMetricsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();

            try
            {
                // List devices metrics
                apiInstance.GetDevicesMetrics();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.GetDevicesMetrics: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatedevicebyid"></a>
# **UpdateDeviceById**
> DeviceResource UpdateDeviceById (string id, DeviceResource deviceResource)

Update device by Id

Update a device based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateDeviceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var id = id_example;  // string | The resource id
            var deviceResource = new DeviceResource(); // DeviceResource | Device resource

            try
            {
                // Update device by Id
                DeviceResource result = apiInstance.UpdateDeviceById(id, deviceResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.UpdateDeviceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 
 **deviceResource** | [**DeviceResource**](DeviceResource.md)| Device resource | 

### Return type

[**DeviceResource**](DeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatedevicesalerts"></a>
# **UpdateDevicesAlerts**
> DeviceResource UpdateDevicesAlerts (AlertResource alertResource)

Update the alert status for a device

Updates the alert status for a specific device for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateDevicesAlertsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new DevicesApi();
            var alertResource = new AlertResource(); // AlertResource | The device's requested alert status

            try
            {
                // Update the alert status for a device
                DeviceResource result = apiInstance.UpdateDevicesAlerts(alertResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling DevicesApi.UpdateDevicesAlerts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertResource** | [**AlertResource**](AlertResource.md)| The device&#39;s requested alert status | 

### Return type

[**DeviceResource**](DeviceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

