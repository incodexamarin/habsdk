# HabSDK.Model.GroupResource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**Members** | [**List&lt;MemberResource&gt;**](MemberResource.md) |  | [optional] 
**ChangedOn** | **long?** | The date/time this resource was updated in seconds since unix epoch | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

