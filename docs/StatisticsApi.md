# HabSDK.Api.StatisticsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetKnioData**](StatisticsApi.md#getkniodata) | **GET** /statistics/knio | 


<a name="getkniodata"></a>
# **GetKnioData**
> KnioResultResource GetKnioData (string filter, string aggregate, long? startDate, long? endDate)



Returns a set of analytics data by place

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetKnioDataExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new StatisticsApi();
            var filter = filter_example;  // string | A place id
            var aggregate = aggregate_example;  // string | a knio category property
            var startDate = 789;  // long? | A date some time in the past formated as unix seconds
            var endDate = 789;  // long? | A date some time in the past or present, formated as unix seconds

            try
            {
                KnioResultResource result = apiInstance.GetKnioData(filter, aggregate, startDate, endDate);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling StatisticsApi.GetKnioData: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A place id | 
 **aggregate** | **string**| a knio category property | 
 **startDate** | **long?**| A date some time in the past formated as unix seconds | 
 **endDate** | **long?**| A date some time in the past or present, formated as unix seconds | 

### Return type

[**KnioResultResource**](KnioResultResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

