# HabSDK.Model.KnioDataPoint
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Date** | **long?** | the data point&#39;s aggregate day | [optional] 
**Val** | **string** | Can be any primitive type &#x60;string, float, int&#x60; | [optional] 
**Target** | **string** | Can be any primitive type &#x60;string, float, int&#x60; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

