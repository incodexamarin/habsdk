# HabSDK.Api.ProfilesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateProfile**](ProfilesApi.md#createprofile) | **POST** /profiles | Create a profile
[**DeleteProfileById**](ProfilesApi.md#deleteprofilebyid) | **DELETE** /profiles/{id} | Delete profile by Id
[**GetProfileById**](ProfilesApi.md#getprofilebyid) | **GET** /profiles/{id} | 
[**GetProfiles**](ProfilesApi.md#getprofiles) | **GET** /profiles | 
[**GetProfilesHistory**](ProfilesApi.md#getprofileshistory) | **GET** /profiles/history | 
[**GetProfilesMetrics**](ProfilesApi.md#getprofilesmetrics) | **PUT** /profiles/metrics | 
[**ResetProfilesZonzes**](ProfilesApi.md#resetprofileszonzes) | **PUT** /profiles/{id}/places/reset | 
[**UpdateProfileById**](ProfilesApi.md#updateprofilebyid) | **PUT** /profiles/{id} | 


<a name="createprofile"></a>
# **CreateProfile**
> ProfileResource CreateProfile (ProfileResource profileResource = null)

Create a profile

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreateProfileExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var profileResource = new ProfileResource(); // ProfileResource | The new profile (optional) 

            try
            {
                // Create a profile
                ProfileResource result = apiInstance.CreateProfile(profileResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.CreateProfile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileResource** | [**ProfileResource**](ProfileResource.md)| The new profile | [optional] 

### Return type

[**ProfileResource**](ProfileResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteprofilebyid"></a>
# **DeleteProfileById**
> void DeleteProfileById (string id)

Delete profile by Id

Delete a profile based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class DeleteProfileByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Delete profile by Id
                apiInstance.DeleteProfileById(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.DeleteProfileById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getprofilebyid"></a>
# **GetProfileById**
> ProfileResource GetProfileById (string id)



Returns profile based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetProfileByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var id = id_example;  // string | The resource id

            try
            {
                ProfileResource result = apiInstance.GetProfileById(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.GetProfileById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**ProfileResource**](ProfileResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getprofiles"></a>
# **GetProfiles**
> PageResourceProfileResource GetProfiles (string filter = null, int? size = null, int? page = null, string order = null)



Returns the list of profiles for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetProfilesExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: `.string`, `.number`, `.boolean`. Add one of these types to the end of the property to define the value type. I.E. `filter=battery_level.number:100|climate_mode.string:heat` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                PageResourceProfileResource result = apiInstance.GetProfiles(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.GetProfiles: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the end of the property to define the value type. I.E. &#x60;filter&#x3D;battery_level.number:100|climate_mode.string:heat&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourceProfileResource**](PageResourceProfileResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getprofileshistory"></a>
# **GetProfilesHistory**
> PageResourceHistoryResource GetProfilesHistory (string filter = null)



Returns a list of profile changes for a time range

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetProfilesHistoryExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var filter = filter_example;  // string | A pipe separated list of start_date and end_date in seconds. I.E. `filter=start_date:1212212|end_date:9898989` filter defaults to the last 24 hours. (optional) 

            try
            {
                PageResourceHistoryResource result = apiInstance.GetProfilesHistory(filter);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.GetProfilesHistory: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of start_date and end_date in seconds. I.E. &#x60;filter&#x3D;start_date:1212212|end_date:9898989&#x60; filter defaults to the last 24 hours. | [optional] 

### Return type

[**PageResourceHistoryResource**](PageResourceHistoryResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getprofilesmetrics"></a>
# **GetProfilesMetrics**
> void GetProfilesMetrics ()



Getting all the metrics of profiles.

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetProfilesMetricsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();

            try
            {
                apiInstance.GetProfilesMetrics();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.GetProfilesMetrics: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="resetprofileszonzes"></a>
# **ResetProfilesZonzes**
> void ResetProfilesZonzes (string id)



Reset all places related to a profile back to managed mode.

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class ResetProfilesZonzesExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var id = id_example;  // string | The resource id

            try
            {
                apiInstance.ResetProfilesZonzes(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.ResetProfilesZonzes: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateprofilebyid"></a>
# **UpdateProfileById**
> ProfileResource UpdateProfileById (string id, ProfileResource profileResource)



Update a profile based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateProfileByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ProfilesApi();
            var id = id_example;  // string | The resource id
            var profileResource = new ProfileResource(); // ProfileResource | Profile resource

            try
            {
                ProfileResource result = apiInstance.UpdateProfileById(id, profileResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfilesApi.UpdateProfileById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 
 **profileResource** | [**ProfileResource**](ProfileResource.md)| Profile resource | 

### Return type

[**ProfileResource**](ProfileResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

