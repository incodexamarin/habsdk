# HabSDK.Model.Property
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Map** | **Object** | the value of a map type property. Only include if your type field is &#x60;map&#x60; | [optional] 
**Values** | **List&lt;Object&gt;** | the value of a list type property. Only include if your type field is &#x60;list&#x60; | [optional] 
**Type** | **string** | The type of the property. Used for polymorphic type recognition and thus must match an expected type with additional properties. available types    \&quot;audio\&quot;, \&quot;audio_group\&quot;, \&quot;boolean\&quot;, \&quot;date\&quot;, \&quot;double\&quot;, \&quot;file\&quot;,    \&quot;file_group\&quot;, \&quot;formatted_text\&quot;, \&quot;image\&quot;, \&quot;image_group\&quot;, \&quot;integer\&quot;, \&quot;list\&quot;,    \&quot;long\&quot;, \&quot;map\&quot;, \&quot;text\&quot;, \&quot;video\&quot;, \&quot;video_group\&quot; | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

