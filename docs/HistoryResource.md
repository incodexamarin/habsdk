# HabSDK.Model.HistoryResource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** | the id of the device | [optional] 
**Name** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**LocationId** | **string** |  | [optional] 
**LocationName** | **string** |  | [optional] 
**LocationType** | **string** |  | [optional] 
**ParentLocationId** | **string** |  | [optional] 
**ParentLocationName** | **string** |  | [optional] 
**ParentLocationType** | **string** |  | [optional] 
**CreatedAt** | **long?** | The date/time this resource was created in seconds since unix epoch | [optional] 
**UpdatedAt** | **long?** | The date/time this resource was updated in seconds since unix epoch | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

