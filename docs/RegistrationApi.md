# HabSDK.Api.RegistrationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RegisterAdmin**](RegistrationApi.md#registeradmin) | **POST** /register/admin | 


<a name="registeradmin"></a>
# **RegisterAdmin**
> AdminResource RegisterAdmin (AdminResource adminResource = null)



Registers an admin user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class RegisterAdminExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new RegistrationApi();
            var adminResource = new AdminResource(); // AdminResource | The user resource object (optional) 

            try
            {
                AdminResource result = apiInstance.RegisterAdmin(adminResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RegistrationApi.RegisterAdmin: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminResource** | [**AdminResource**](AdminResource.md)| The user resource object | [optional] 

### Return type

[**AdminResource**](AdminResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

