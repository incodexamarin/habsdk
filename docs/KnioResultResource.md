# HabSDK.Model.KnioResultResource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ChartData** | [**List&lt;KnioDataPoint&gt;**](KnioDataPoint.md) |  | [optional] 
**Labels** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

