# HabSDK.Api.TenantsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateTenant**](TenantsApi.md#createtenant) | **POST** /tenants | Create a tenant
[**GetTenant**](TenantsApi.md#gettenant) | **GET** /tenants/{id} | 
[**GetTenants**](TenantsApi.md#gettenants) | **GET** /tenants | 


<a name="createtenant"></a>
# **CreateTenant**
> TenantResource CreateTenant (TenantResource tenantResource = null)

Create a tenant

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreateTenantExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new TenantsApi();
            var tenantResource = new TenantResource(); // TenantResource | The new tenant (optional) 

            try
            {
                // Create a tenant
                TenantResource result = apiInstance.CreateTenant(tenantResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TenantsApi.CreateTenant: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantResource** | [**TenantResource**](TenantResource.md)| The new tenant | [optional] 

### Return type

[**TenantResource**](TenantResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettenant"></a>
# **GetTenant**
> TenantResource GetTenant (string id)



Returns matched tenant id

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetTenantExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new TenantsApi();
            var id = id_example;  // string | The resource id

            try
            {
                TenantResource result = apiInstance.GetTenant(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TenantsApi.GetTenant: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**TenantResource**](TenantResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="gettenants"></a>
# **GetTenants**
> PageResourceTenantResource GetTenants (string filter = null, int? size = null, int? page = null, string order = null)



Returns the list of tenants for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetTenantsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new TenantsApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: `.string`, `.number`, `.boolean`. Add one of these types to the end of the property to define the value type. I.E. `filter=battery_level.number:100|climate_mode.string:heat` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                PageResourceTenantResource result = apiInstance.GetTenants(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TenantsApi.GetTenants: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the end of the property to define the value type. I.E. &#x60;filter&#x3D;battery_level.number:100|climate_mode.string:heat&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourceTenantResource**](PageResourceTenantResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

