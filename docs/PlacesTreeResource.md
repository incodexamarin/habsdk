# HabSDK.Model.PlacesTreeResource
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** | the id of the Place | [optional] 
**Name** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Parent** | **Guid?** | the id of the parent Place | [optional] 
**Status** | **string** |  | [optional] 
**Tags** | **List&lt;string&gt;** |  | [optional] 
**ClimateMode** | **string** |  | [optional] 
**AutoMode** | **string** |  | [optional] 
**CoolMin** | **decimal?** |  | [optional] 
**CoolMax** | **decimal?** |  | [optional] 
**CoolTarget** | **decimal?** |  | [optional] 
**HeatMin** | **decimal?** |  | [optional] 
**HeatMax** | **decimal?** |  | [optional] 
**HeatTarget** | **decimal?** |  | [optional] 
**SmartMin** | **decimal?** |  | [optional] 
**SmartMax** | **decimal?** |  | [optional] 
**Connected** | **bool?** |  | [optional] 
**CurrentTemp** | **decimal?** |  | [optional] 
**DefaultProfile** | **bool?** |  | [optional] 
**FanSpeed** | **string** |  | [optional] 
**ActivityStatus** | **string** |  | [optional] 
**Runtime** | **decimal?** |  | [optional] 
**Locked** | **bool?** |  | [optional] 
**ProfileId** | **decimal?** |  | [optional] 
**ProfileName** | **string** |  | [optional] 
**Occupied** | **bool?** |  | [optional] 
**LastUpdated** | **long?** |  | [optional] 
**ParentLocationName** | **string** |  | [optional] 
**AlertState** | **string** |  | [optional] 
**CreatedAt** | **long?** | The date/time this resource was created in seconds since unix epoch | [optional] 
**UpdatedAt** | **long?** | The date/time this resource was updated in seconds since unix epoch | [optional] 
**Children** | [**List&lt;PlacesTreeNodeResource&gt;**](PlacesTreeNodeResource.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

