# HabSDK.Api.ResidentsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateResident**](ResidentsApi.md#createresident) | **POST** /residents | Create a resident
[**DeleteResidentById**](ResidentsApi.md#deleteresidentbyid) | **DELETE** /residents/{id} | Delete resident by Id
[**GetResident**](ResidentsApi.md#getresident) | **GET** /residents/{id} | 
[**GetResidentPlaces**](ResidentsApi.md#getresidentplaces) | **GET** /residents/{id}/places | List a Resident&#39;s Places
[**GetResidents**](ResidentsApi.md#getresidents) | **GET** /residents | 
[**UpdateResident**](ResidentsApi.md#updateresident) | **PUT** /residents/{id} | Update a resident
[**UpdateResidentPlace**](ResidentsApi.md#updateresidentplace) | **PUT** /residents/{id}/places/{place_id} | Update a Resident&#39;s Place by Id
[**UpdateResidentStatus**](ResidentsApi.md#updateresidentstatus) | **PUT** /residents/{id}/status | Update a resident status by Id


<a name="createresident"></a>
# **CreateResident**
> ResidentResource CreateResident (ResidentResource residentResource = null)

Create a resident

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreateResidentExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var residentResource = new ResidentResource(); // ResidentResource | The new resident (optional) 

            try
            {
                // Create a resident
                ResidentResource result = apiInstance.CreateResident(residentResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.CreateResident: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **residentResource** | [**ResidentResource**](ResidentResource.md)| The new resident | [optional] 

### Return type

[**ResidentResource**](ResidentResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteresidentbyid"></a>
# **DeleteResidentById**
> void DeleteResidentById (string id)

Delete resident by Id

Delete a resident based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class DeleteResidentByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Delete resident by Id
                apiInstance.DeleteResidentById(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.DeleteResidentById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getresident"></a>
# **GetResident**
> ResidentResource GetResident (string id)



Returns matched resident id

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetResidentExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var id = id_example;  // string | The resource id

            try
            {
                ResidentResource result = apiInstance.GetResident(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.GetResident: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**ResidentResource**](ResidentResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getresidentplaces"></a>
# **GetResidentPlaces**
> PageResourcePlaceResource GetResidentPlaces ()

List a Resident's Places

Returns the list of places for the requested resident

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetResidentPlacesExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();

            try
            {
                // List a Resident's Places
                PageResourcePlaceResource result = apiInstance.GetResidentPlaces();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.GetResidentPlaces: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PageResourcePlaceResource**](PageResourcePlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getresidents"></a>
# **GetResidents**
> PageResourceResidentResource GetResidents (string filter = null, int? size = null, int? page = null, string order = null)



Returns the list of residents for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetResidentsExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: `.string`, `.number`, `.boolean`. Add one of these types to the end of the property to define the value type. I.E. `filter=battery_level.number:100|climate_mode.string:heat` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                PageResourceResidentResource result = apiInstance.GetResidents(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.GetResidents: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the end of the property to define the value type. I.E. &#x60;filter&#x3D;battery_level.number:100|climate_mode.string:heat&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourceResidentResource**](PageResourceResidentResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateresident"></a>
# **UpdateResident**
> void UpdateResident (ResidentResource residentResource = null)

Update a resident

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateResidentExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var residentResource = new ResidentResource(); // ResidentResource | The resident (optional) 

            try
            {
                // Update a resident
                apiInstance.UpdateResident(residentResource);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.UpdateResident: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **residentResource** | [**ResidentResource**](ResidentResource.md)| The resident | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateresidentplace"></a>
# **UpdateResidentPlace**
> void UpdateResidentPlace (string id, UpdateResidentPlaceResource updateResidentPlaceResource)

Update a Resident's Place by Id

Update a place for a resident

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateResidentPlaceExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var id = id_example;  // string | The resource id
            var updateResidentPlaceResource = new UpdateResidentPlaceResource(); // UpdateResidentPlaceResource | Properties a Resident is allowed to update

            try
            {
                // Update a Resident's Place by Id
                apiInstance.UpdateResidentPlace(id, updateResidentPlaceResource);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.UpdateResidentPlace: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 
 **updateResidentPlaceResource** | [**UpdateResidentPlaceResource**](UpdateResidentPlaceResource.md)| Properties a Resident is allowed to update | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateresidentstatus"></a>
# **UpdateResidentStatus**
> void UpdateResidentStatus (string id, StatusResource statusResource)

Update a resident status by Id

Update a resident's status based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdateResidentStatusExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new ResidentsApi();
            var id = id_example;  // string | The resource id
            var statusResource = new StatusResource(); // StatusResource | The residents requested status

            try
            {
                // Update a resident status by Id
                apiInstance.UpdateResidentStatus(id, statusResource);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ResidentsApi.UpdateResidentStatus: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 
 **statusResource** | [**StatusResource**](StatusResource.md)| The residents requested status | 

### Return type

void (empty response body)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

