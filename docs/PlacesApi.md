# HabSDK.Api.PlacesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreatePlace**](PlacesApi.md#createplace) | **POST** /places | Create a place
[**CreatePlacesTree**](PlacesApi.md#createplacestree) | **POST** /places/tree | Create place tree
[**DeletePlaceById**](PlacesApi.md#deleteplacebyid) | **DELETE** /places/{id} | Delete place by id
[**GetPlaceById**](PlacesApi.md#getplacebyid) | **GET** /places/{id} | Get place by id
[**GetPlaces**](PlacesApi.md#getplaces) | **GET** /places | Get places
[**GetPlacesTree**](PlacesApi.md#getplacestree) | **GET** /places/tree/{root} | Get place tree
[**UpdatePlaceById**](PlacesApi.md#updateplacebyid) | **PUT** /places/{id} | Update place by id


<a name="createplace"></a>
# **CreatePlace**
> PlaceResource CreatePlace (PlaceResource placeResource = null)

Create a place

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreatePlaceExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var placeResource = new PlaceResource(); // PlaceResource | The new place (optional) 

            try
            {
                // Create a place
                PlaceResource result = apiInstance.CreatePlace(placeResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.CreatePlace: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **placeResource** | [**PlaceResource**](PlaceResource.md)| The new place | [optional] 

### Return type

[**PlaceResource**](PlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createplacestree"></a>
# **CreatePlacesTree**
> PlacesTreeResource CreatePlacesTree (PlacesTreeResource treeResource)

Create place tree

Returns a tree of the requesting user's places.

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class CreatePlacesTreeExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var treeResource = new PlacesTreeResource(); // PlacesTreeResource | Place resource

            try
            {
                // Create place tree
                PlacesTreeResource result = apiInstance.CreatePlacesTree(treeResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.CreatePlacesTree: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **treeResource** | [**PlacesTreeResource**](PlacesTreeResource.md)| Place resource | 

### Return type

[**PlacesTreeResource**](PlacesTreeResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteplacebyid"></a>
# **DeletePlaceById**
> PlaceResource DeletePlaceById (string id)

Delete place by id

Delete a place based on id provided.

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class DeletePlaceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Delete place by id
                PlaceResource result = apiInstance.DeletePlaceById(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.DeletePlaceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**PlaceResource**](PlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getplacebyid"></a>
# **GetPlaceById**
> PlaceResource GetPlaceById (string id)

Get place by id

Returns place based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetPlaceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var id = id_example;  // string | The resource id

            try
            {
                // Get place by id
                PlaceResource result = apiInstance.GetPlaceById(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.GetPlaceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 

### Return type

[**PlaceResource**](PlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getplaces"></a>
# **GetPlaces**
> PageResourcePlaceResource GetPlaces (string filter = null, int? size = null, int? page = null, string order = null)

Get places

Returns the list of profiles for the requested user

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetPlacesExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var filter = filter_example;  // string | A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: `.string`, `.number`, `.boolean`. Add one of these types to the end of the property to define the value type. I.E. `filter=battery_level.number:100|climate_mode.string:heat` (optional) 
            var size = 56;  // int? | The number of objects returned per page (optional)  (default to 25)
            var page = 56;  // int? | The number of the page returned, starting with 1 (optional)  (default to 1)
            var order = order_example;  // string | A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  `.string`, `.number`, `.boolean`. Add one of these types to the  end of the property to define the value type.   I.E. `sort=battery_level.number:asc` (optional)  (default to id:asc)

            try
            {
                // Get places
                PageResourcePlaceResource result = apiInstance.GetPlaces(filter, size, page, order);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.GetPlaces: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **string**| A pipe separated list of filter requirements. Each entry matching PROPERTY_NAME:FILTER_VALUE. Filter for resource by any field using dot syntax for nested fields. There are three available property types: &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the end of the property to define the value type. I.E. &#x60;filter&#x3D;battery_level.number:100|climate_mode.string:heat&#x60; | [optional] 
 **size** | **int?**| The number of objects returned per page | [optional] [default to 25]
 **page** | **int?**| The number of the page returned, starting with 1 | [optional] [default to 1]
 **order** | **string**| A pipe separated list of sorting requirements, each entry matching PROPERTY_NAME:[asc|desc]  Filter for resource by any field using dot syntax for  nested fields. There are three available property types:  &#x60;.string&#x60;, &#x60;.number&#x60;, &#x60;.boolean&#x60;. Add one of these types to the  end of the property to define the value type.   I.E. &#x60;sort&#x3D;battery_level.number:asc&#x60; | [optional] [default to id:asc]

### Return type

[**PageResourcePlaceResource**](PageResourcePlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getplacestree"></a>
# **GetPlacesTree**
> PlacesTreeResource GetPlacesTree (string root)

Get place tree

Returns a tree of the requesting user's places.

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class GetPlacesTreeExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var root = root_example;  // string | The starting resource id or `root`. if the `id == 'root'` the users `root_location` parameter will be the starting point of the tree.

            try
            {
                // Get place tree
                PlacesTreeResource result = apiInstance.GetPlacesTree(root);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.GetPlacesTree: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **root** | **string**| The starting resource id or &#x60;root&#x60;. if the &#x60;id &#x3D;&#x3D; &#39;root&#39;&#x60; the users &#x60;root_location&#x60; parameter will be the starting point of the tree. | 

### Return type

[**PlacesTreeResource**](PlacesTreeResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateplacebyid"></a>
# **UpdatePlaceById**
> PlaceResource UpdatePlaceById (string id, PlaceResource placeResource)

Update place by id

Update a place based on id provided

### Example
```csharp
using System;
using System.Diagnostics;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;

namespace Example
{
    public class UpdatePlaceByIdExample
    {
        public void main()
        {
            // Configure OAuth2 access token for authorization: oauth2_client_credentials_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";
            // Configure OAuth2 access token for authorization: oauth2_password_grant
            Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

            var apiInstance = new PlacesApi();
            var id = id_example;  // string | The resource id
            var placeResource = new PlaceResource(); // PlaceResource | Place resource

            try
            {
                // Update place by id
                PlaceResource result = apiInstance.UpdatePlaceById(id, placeResource);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PlacesApi.UpdatePlaceById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| The resource id | 
 **placeResource** | [**PlaceResource**](PlaceResource.md)| Place resource | 

### Return type

[**PlaceResource**](PlaceResource.md)

### Authorization

[oauth2_client_credentials_grant](../README.md#oauth2_client_credentials_grant), [oauth2_password_grant](../README.md#oauth2_password_grant)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

