/* 
 * HabAPI
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using HabSDK.Api;
using HabSDK.Model;
using HabSDK.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace HabSDK.Test
{
    /// <summary>
    ///  Class for testing AlertResource1
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class AlertResource1Tests
    {
        // TODO uncomment below to declare an instance variable for AlertResource1
        //private AlertResource1 instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of AlertResource1
            //instance = new AlertResource1();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of AlertResource1
        /// </summary>
        [Test]
        public void AlertResource1InstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" AlertResource1
            //Assert.IsInstanceOfType<AlertResource1> (instance, "variable 'instance' is a AlertResource1");
        }


        /// <summary>
        /// Test the property 'Message'
        /// </summary>
        [Test]
        public void MessageTest()
        {
            // TODO unit test for the property 'Message'
        }

    }

}
